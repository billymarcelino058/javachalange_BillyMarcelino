package ist.challenge.Billy_Marcelino_Alexander_Dimas_Wiryawan.controller;

import ist.challenge.Billy_Marcelino_Alexander_Dimas_Wiryawan.dto.UserRequestDTO;
import ist.challenge.Billy_Marcelino_Alexander_Dimas_Wiryawan.entity.User;
import ist.challenge.Billy_Marcelino_Alexander_Dimas_Wiryawan.handler.ResponseHandler;
import ist.challenge.Billy_Marcelino_Alexander_Dimas_Wiryawan.repository.UserRepository;
import ist.challenge.Billy_Marcelino_Alexander_Dimas_Wiryawan.service.UserService;
import ist.challenge.Billy_Marcelino_Alexander_Dimas_Wiryawan.service.UserServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class UserController {

    private final AuthenticationManager authenticationManager;
    private final UserRepository userRepository;

    @Autowired
    UserService userService;

    @GetMapping("/user")
    public ResponseEntity<?> getAllUser()
    {
        return userService.getAllUser();
    }

    @PutMapping("/user/update/{id}")
    public ResponseEntity<?> updateUser(@RequestBody UserRequestDTO userRequestDTO, @PathVariable("id") Long id)
    {
        return userService.updateUser(userRequestDTO,id);
    }

    @PostMapping("/login")
    public  ResponseEntity<?> login(@RequestBody UserRequestDTO userRequestDTO)
    {
        return userService.login(userRequestDTO);
    }

    @PostMapping("/register")
    public  ResponseEntity<?> Register(@RequestBody UserRequestDTO userRequestDTO){
        return  userService.Register(userRequestDTO);
    }
}
