package ist.challenge.Billy_Marcelino_Alexander_Dimas_Wiryawan.service;
import ist.challenge.Billy_Marcelino_Alexander_Dimas_Wiryawan.dto.UserRequestDTO;
import org.springframework.http.ResponseEntity;


public interface UserService {

    ResponseEntity<?> getAllUser();

    ResponseEntity<?> updateUser(UserRequestDTO userRequestDTO, Long id) ;

    public  ResponseEntity<?> login(UserRequestDTO userRequestDTO);

    public  ResponseEntity<?> Register(UserRequestDTO userRequestDTO);
}
