package ist.challenge.Billy_Marcelino_Alexander_Dimas_Wiryawan.service;

import ist.challenge.Billy_Marcelino_Alexander_Dimas_Wiryawan.entity.MyUserDetail;
import ist.challenge.Billy_Marcelino_Alexander_Dimas_Wiryawan.entity.User;
import ist.challenge.Billy_Marcelino_Alexander_Dimas_Wiryawan.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.transaction.Transactional;

public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username).orElseThrow(()->new UsernameNotFoundException("User not Found"));
        return new MyUserDetail(user);
    }
}
