package ist.challenge.Billy_Marcelino_Alexander_Dimas_Wiryawan.service;

import ist.challenge.Billy_Marcelino_Alexander_Dimas_Wiryawan.dto.UserRequestDTO;
import ist.challenge.Billy_Marcelino_Alexander_Dimas_Wiryawan.dto.UserResponseDTO;
import ist.challenge.Billy_Marcelino_Alexander_Dimas_Wiryawan.entity.User;
import ist.challenge.Billy_Marcelino_Alexander_Dimas_Wiryawan.handler.ResponseHandler;
import ist.challenge.Billy_Marcelino_Alexander_Dimas_Wiryawan.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.List;


@Service
public class UserServiceImpl implements UserService{

    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthenticationManager authenticationManager;

    @Override
    public ResponseEntity<?> getAllUser() {
       try{
            List<User> userList = userRepository.findAll();
            List<UserResponseDTO> userResponseDTO = new ArrayList<>();
            for (User user : userList){
                userResponseDTO.add(user.converToResponse());
            }
            return ResponseHandler.generateResponse("All Data User",HttpStatus.OK, userResponseDTO);
       }catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.NOT_FOUND,"data tidak di temukan");
       }
    }

    @Override
    public ResponseEntity<?> updateUser(UserRequestDTO userRequestDTO, Long id) {
        try {
            User getUser = userRepository.findById(id).orElseThrow(()->new UsernameNotFoundException("user not found"));
            getUser.setUsername(userRequestDTO.getUsername());
            getUser.setPassword(userRequestDTO.getPassword());
            User updatedUser = this.userRepository.save(getUser);
            return ResponseHandler.generateResponse("user" + updatedUser.getId() + "Updated Success", HttpStatus.CREATED, updatedUser);
        }catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.CONFLICT, "“Password tidak boleh sama dengan password sebelumnya");
        }
    }

    @Override
    public  ResponseEntity<?> login(@RequestBody UserRequestDTO userRequestDTO)
    {
        try {
            Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userRequestDTO.getUsername(), userRequestDTO.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            User user = userRepository.findByUsername(userRequestDTO.getUsername()).orElseThrow(()->new UsernameNotFoundException("User not Found"));
            return ResponseHandler.generateResponse("Succes login", HttpStatus.OK, user);
        }catch (Exception e){
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.BAD_REQUEST, "Username dan / atau password kosong");
        }
    }

    @Override
    public ResponseEntity<?> Register(UserRequestDTO userRequestDTO)
    {
        try{
        User user = userRequestDTO.ConverToEntity();
        userRepository.save(user);
        UserResponseDTO userResult = user.converToResponse();
        return ResponseHandler.generateResponse("Register Success", HttpStatus.CREATED, userResult);
        }catch (Exception e){
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.CONFLICT, "Username Sudah Terpakai");
        }
    }
}
