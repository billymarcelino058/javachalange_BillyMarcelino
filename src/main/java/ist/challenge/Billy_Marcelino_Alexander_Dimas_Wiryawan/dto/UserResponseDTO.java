package ist.challenge.Billy_Marcelino_Alexander_Dimas_Wiryawan.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class UserResponseDTO {

    private Long id;

    private String username;

    private String password;
}
