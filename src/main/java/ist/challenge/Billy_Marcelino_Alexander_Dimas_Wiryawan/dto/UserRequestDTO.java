package ist.challenge.Billy_Marcelino_Alexander_Dimas_Wiryawan.dto;

import ist.challenge.Billy_Marcelino_Alexander_Dimas_Wiryawan.entity.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserRequestDTO {

    private String username;

    private String password;

    @Override
    public String toString() {
        return "UserRequestDTO{" +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public User ConverToEntity(){
        return User.builder()
                .username(this.username)
                .password(this.password)
                .build();
    }
}
