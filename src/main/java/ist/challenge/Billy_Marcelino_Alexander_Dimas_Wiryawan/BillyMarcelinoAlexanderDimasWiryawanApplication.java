package ist.challenge.Billy_Marcelino_Alexander_Dimas_Wiryawan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BillyMarcelinoAlexanderDimasWiryawanApplication {

	public static void main(String[] args) {
		SpringApplication.run(BillyMarcelinoAlexanderDimasWiryawanApplication.class, args);
	}
}
