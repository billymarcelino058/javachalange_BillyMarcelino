package ist.challenge.Billy_Marcelino_Alexander_Dimas_Wiryawan.entity;

import ist.challenge.Billy_Marcelino_Alexander_Dimas_Wiryawan.dto.UserResponseDTO;
import lombok.*;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = "user", schema = "public", uniqueConstraints = {@UniqueConstraint(columnNames = "username")})
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String username;

    private String password;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public UserResponseDTO converToResponse(){
        return UserResponseDTO.builder()
                .id(this.id)
                .username(this.username)
                .password(this.password)
                .build();
    }
}
