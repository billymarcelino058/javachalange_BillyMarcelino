package ist.challenge.Billy_Marcelino_Alexander_Dimas_Wiryawan.handler;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;

public class ResponseHandler {
    public static ResponseEntity<Object> generateResponse(String errorMessage, HttpStatus status, Object response){
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("status", status.value());
        responseMap.put("error_message", errorMessage);
        responseMap.put("data", response);
        return new ResponseEntity<Object>(responseMap, status);
    }
}
