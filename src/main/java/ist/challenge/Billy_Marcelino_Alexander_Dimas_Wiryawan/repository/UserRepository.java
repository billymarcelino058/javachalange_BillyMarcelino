package ist.challenge.Billy_Marcelino_Alexander_Dimas_Wiryawan.repository;

import ist.challenge.Billy_Marcelino_Alexander_Dimas_Wiryawan.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);
}
